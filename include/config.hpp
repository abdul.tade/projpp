#include <iostream>
#include <map>
#include <vector>
#include <fstream>
#include <sstream>

class Config
{
public:

    static std::vector<std::string> readlines(const std::string &filename) {
        std::vector<std::string> lines{};
        std::string line{};
        std::ifstream file{};

        file.open(filename, std::ios::binary | std::ios::in);

        while(std::getline(file,line))
        {
            lines.emplace_back(line);
        }

        file.close();

        return lines;
    }

    static auto split(const std::string &str, char delim)
    {
        std::string token{};
        std::vector<std::string> tokens{};
        std::stringstream ss{str};

        while (std::getline(ss,token, delim))
        {
            tokens.emplace_back(token);
        }

        return tokens;
    }

    Config() = delete;

    Config(const std::string &configPath = "proj.cfg")
        : configPath_(configPath)
    {}

    void readConfig()
    {
        auto lines = Config::readlines(configPath_); 
        for (auto const& v : lines) {
            auto tokens = Config::split(v, '=');
            if (tokens.size() != 2) {
                throw std::invalid_argument{"invalid config format: [FORMAT] key=value"};
            }
            configData_[tokens[0]] = tokens[1];
        }
    }

    bool exists(const std::string &key)
    {
        return configData_.find(key) != configData_.end();
    }

    std::string &get(const std::string &key)
    {
        if (not exists(key)) {
            throw std::invalid_argument{"key " + key + " cannot be found"};
        }
        return configData_[key];
    }

    std::string &operator[](const std::string &key) {
        return get(key);
    }

    void toConfigFile() {

    }

private:
    std::string configPath_;
    std::map<std::string, std::string> configData_;

};