#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream>
#include <unistd.h>
#include <filesystem>
#include "include/config.hpp"

std::string usage()
{
    return "usage: not enough arguments\nproj++ [COMMAND] [ARG0] [ARG1] ...";
}

void availableCommands()
{
    std::cout << "Available command types are "
                 "\n\tcommands:\n\t\tinit\n\tbuild";
}

void write(const std::string &filepath, const std::string &str)
{
    std::fstream file;
    file.open(filepath, std::ios::binary | std::ios::out);
    file.write(str.c_str(), str.size());
    file.close();
}

void makeFile(const std::string &dir)
{
    auto file = dir + "/Makefile";
    std::string message = "CXX := g++\n"
                          "CXXFLAGS := -O3 -Wall\n"
                          "\n"
                          "\n"
                          ".PHONY: all build run\n"
                          "\n"
                          "\n"
                          "all:\n"
                          "\tmake build\n"
                          "\tmake run\n"
                          "\n"
                          "\n"
                          "build: src/main.cpp build/ \n"
                          "\t$(CXX) $(CXXFLAGS) -o build/main src/main.cpp"
                          " -fno-rtti -march=native -flto -fwhole-program -fprofile-generate -std=c++20\n"
                          "\n"
                          "\n"
                          "run: build/main\n"
                          "\t./build/main\n"
                          "\n";

    write(file, message);
}

void init(const std::string &prefix)
{
    std::string include = prefix + "/include";
    std::string src = prefix + "/src";
    std::string lib = prefix + "/lib";
    std::string build = prefix + "/build";
    std::string test = prefix + "/test";
    std::string gitignore = prefix + ".gitignore";
    mkdir(include.c_str(), 0777);
    mkdir(src.c_str(), 0777);
    mkdir(lib.c_str(), 0777);
    mkdir(build.c_str(), 0777);
    mkdir(test.c_str(), 0777);
    write(src + "/main.cpp", "#include <iostream>\n\nint main() {\n\tstd::cout << \"Hello world\\n\";\n}");
    write(gitignore, "build/\n");
    makeFile(prefix);
}

void addFile(const std::string &dir, const std::string &filename)
{
    using namespace std::filesystem;

    if (exists(path{dir})) {
    }
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        throw std::invalid_argument{usage()};
    }

    std::string command = argv[1];
    std::string projDir = argv[2];

    if (command == "init")
    {
        std::cout << "initializing cpp project" << projDir << " ...";
        mkdir(argv[2], 0777);
        init(projDir);
    }
    else
    {
        availableCommands();
        throw std::invalid_argument{"unrecognized command type"};
    }
}
